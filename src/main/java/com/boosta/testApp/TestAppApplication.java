package com.boosta.testApp;


import com.boosta.testApp.service.ThreadServiceBridge;
import com.boosta.testApp.service.UrlService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

@SpringBootApplication
@EnableJpaRepositories
public class TestAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestAppApplication.class, args);
		UrlService urlService = ThreadServiceBridge.services().getUrlService();
		urlService.initDataBase();
		List<String> urls = new UrlGetter().getUrls();

		try {
			for (int i = 0; i < urls.size(); i++){
				ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);

				UrlThreads urlThreads = new UrlThreads(urls.get(i));

				executor.execute(urlThreads);
				if((i+1)%10==0){
					Thread.sleep(1000);
				}
			}
		} catch(InterruptedException e) {
			e.printStackTrace();
		}


	}
}
