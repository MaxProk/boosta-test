package com.boosta.testApp.domain;

import javax.persistence.*;

@Entity
@Table(name = "Url")
public class Url {

    public Url() {}

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true)
    private String url;

    public Url(String url){
        this.url = url;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return String.format(
                "Url[id=%d, url='%s",
                id, url);
    }
}

