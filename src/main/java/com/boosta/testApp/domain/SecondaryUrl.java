package com.boosta.testApp.domain;

import javax.persistence.*;

@Entity
@Table(name = "secondary_url")
public class SecondaryUrl {

    public SecondaryUrl() {}

    public SecondaryUrl(String childURL, Url url) {
        this.childURL = childURL;
        this.url = url;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String childURL;

    @ManyToOne
    private Url url;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getChildURL() {
        return childURL;
    }

    public void setChildURL(String childURL) {
        this.childURL = childURL;
    }

    public Url getUrl() {
        return url;
    }

    public void setUrl(Url url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "SecondaryUrl{" +
                "id=" + id +
                ", childURL='" + childURL + '\'' +
                '}';
    }
}
