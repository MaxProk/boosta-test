package com.boosta.testApp;

import com.boosta.testApp.service.ThreadServiceBridge;
import com.boosta.testApp.service.UrlService;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;


public class UrlThreads implements Runnable {

    public String url;

    private Path path = Paths.get(".").toAbsolutePath().getParent();

    public UrlThreads(String url) {
        this.url = url;
    }


    @Override
    public void run() {
        try{

                UrlService urlService = ThreadServiceBridge.services().getUrlService();
                Document document = Jsoup.connect(url).get();
                File linksFile = new File(path + "/src/main/resources/links/", Thread.currentThread().getName()+ ".html");

                try (BufferedWriter linksFileWriter = new BufferedWriter(new FileWriter(linksFile, true))) {
                    Connection.Response html = Jsoup.connect(url).execute();
                    linksFileWriter.write(html.body());
                } catch (IOException e){
                    e.printStackTrace();
                }
                Elements linkTags = document.select("a[href]");
                String link;
                try {
                    for (int i = 0; i < linkTags.size(); i++) {
                        link = linkTags.get(i).attr("href");
                        urlService.save(link, url);
                    }
                }catch (IndexOutOfBoundsException e){
                    e.printStackTrace();
                }
        } catch (IOException e) {
                e.printStackTrace();
        }
    }
}