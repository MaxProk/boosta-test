package com.boosta.testApp.controller;

import com.boosta.testApp.domain.SecondaryUrl;
import com.boosta.testApp.domain.Url;
import com.boosta.testApp.repository.SecondaryUrlRepository;
import com.boosta.testApp.repository.UrlRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class TestAppController {
    @Autowired
    private final UrlRepository urlRepository;

    @Autowired
    private final SecondaryUrlRepository secondaryUrlRepository;


    public TestAppController(UrlRepository urlRepository, SecondaryUrlRepository secondaryUrlRepository) {
        this.urlRepository = urlRepository;
        this.secondaryUrlRepository = secondaryUrlRepository;
    }

    @GetMapping("/surls")
    public ResponseEntity<List<SecondaryUrl>> getAllSecondaryUrls() {
        List<SecondaryUrl> data = new ArrayList<>();
        secondaryUrlRepository.findAll().forEach(item -> data.add(item));
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    @GetMapping("urls")
    public  ResponseEntity<List<Url>> getAllUrls() {
        List<Url> data = new ArrayList<>();
        urlRepository.findAll().forEach(item -> data.add(item));
        return new ResponseEntity<>(data, HttpStatus.OK);
    }
}



