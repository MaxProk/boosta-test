package com.boosta.testApp.service;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class ThreadServiceBridge implements SpringContextBridgedServices, ApplicationContextAware {

    @Autowired
    UrlService urlService;

    private static ApplicationContext applicationContext;

    @Override
    public UrlService getUrlService() {
        return urlService;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public static SpringContextBridgedServices services() {
        return applicationContext.getBean(SpringContextBridgedServices.class);
    }
}
