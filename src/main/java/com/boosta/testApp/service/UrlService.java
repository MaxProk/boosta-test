package com.boosta.testApp.service;


import com.boosta.testApp.domain.SecondaryUrl;
import com.boosta.testApp.domain.Url;
import com.boosta.testApp.repository.SecondaryUrlRepository;
import com.boosta.testApp.repository.UrlRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;


@Service
@Transactional
public class UrlService {

    @Autowired
    UrlRepository urlRepository;

    @Autowired
    SecondaryUrlRepository secondaryUrlRepository;

    private Path path = Paths.get(".").toAbsolutePath().getParent();


    public UrlService(UrlRepository urlRepository, SecondaryUrlRepository secondaryUrlRepository){
        this.urlRepository = urlRepository;
        this.secondaryUrlRepository = secondaryUrlRepository;
    }

    public void initDataBase() {
        try {
            File file = new File(path + "/src/main/resources/list.txt");
            FileReader fr = new FileReader(file);
            BufferedReader reader = new BufferedReader(fr);
            String url = reader.readLine();
            while (url != null) {
                Url urlEntity = new Url(url);
                urlRepository.save(urlEntity);
                url = reader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void save(String childUrl, String url){
        Url urlEntity = urlRepository.findByUrl(url);
        SecondaryUrl secondaryUrl = new SecondaryUrl(childUrl, urlEntity);
        secondaryUrlRepository.save(secondaryUrl);
    }
}
