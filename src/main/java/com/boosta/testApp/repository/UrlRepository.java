package com.boosta.testApp.repository;

import com.boosta.testApp.domain.Url;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UrlRepository extends CrudRepository<Url, Long> {
    Url findByUrl(String url);
}
