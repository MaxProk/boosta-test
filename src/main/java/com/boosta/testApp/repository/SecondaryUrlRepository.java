package com.boosta.testApp.repository;

import com.boosta.testApp.domain.SecondaryUrl;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface SecondaryUrlRepository extends CrudRepository<SecondaryUrl, Long> {
}
