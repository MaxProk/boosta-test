package com.boosta.testApp;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class UrlGetter {

    private final Path projectPath = Paths.get(".").toAbsolutePath().getParent();

    public List<String> getUrls() {
        List<String> urls = new ArrayList<>();

        try {
            File file = new File(projectPath + "/src/main/resources/list.txt");
            FileReader fr = new FileReader(file);
            BufferedReader reader = new BufferedReader(fr);
            String url = reader.readLine();
            while (url != null) {
                urls.add(url);
                url = reader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return urls;
    }
}
